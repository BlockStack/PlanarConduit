package com.planarshiftmc.plugin.planarmagic.gson;

/**
 * Interface tied to PostProcessingEnabler causes the gsonPostProcess() method
 * to be called after GSON object construction.
 * 
 * @author wispoffates
 */
public interface GSONPostProcessable {
	/**
	 * This method is called after GSON Object construction;
	 */
	public void gsonPostProcess();
}
