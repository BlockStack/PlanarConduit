package com.planarshiftmc.plugin.planarmagic.events;

import org.bukkit.entity.Player;

import com.planarshiftmc.plugin.planarmagic.companion.Companion;

/**
 * Event triggered when a companion is spawned.
 * 
 * @author troy
 *
 */
public class SpawnCompanionEvent extends CompanionEvent {

	/**
	 * Constructor
	 * 
	 * @param companion
	 *                      The companion spawned.
	 * @param player
	 *                      The companions player.
	 */
	public SpawnCompanionEvent(Companion companion, Player player) {
		super(companion, player);
	}

}
