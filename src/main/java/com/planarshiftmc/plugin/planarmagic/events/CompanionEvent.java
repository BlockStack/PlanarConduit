package com.planarshiftmc.plugin.planarmagic.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.planarshiftmc.plugin.planarmagic.companion.Companion;

/**
 * Base event for companions.
 * 
 * @author wispoffates
 */
public class CompanionEvent extends Event implements Cancellable {
	protected static HandlerList handlers = new HandlerList();

	protected boolean isCancelled = false;

	protected Companion companion;
	protected Player player;

	/**
	 * Constructor
	 * 
	 * @param companion
	 *                      The companion spawned.
	 * @param player
	 *                      The companion's player.
	 */
	public CompanionEvent(Companion companion, Player player) {
		super();
		this.companion = companion;
		this.player = player;
	}

	/**
	 * @return the companion
	 */
	public Companion getCompanion() {
		return this.companion;
	}

	/**
	 * @return the player
	 */
	public Player getPlayer() {
		return this.player;
	}

	@Override
	public HandlerList getHandlers() {
		return CompanionEvent.handlers;
	}

	public static HandlerList getHandlerList() {
		return CompanionEvent.handlers;
	}

	@Override
	public boolean isCancelled() {
		return this.isCancelled;
	}

	@Override
	public void setCancelled(boolean cancel) {
		this.isCancelled = cancel;
	}

}
