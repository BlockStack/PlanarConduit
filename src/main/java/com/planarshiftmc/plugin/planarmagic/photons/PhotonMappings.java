package com.planarshiftmc.plugin.planarmagic.photons;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;

import com.google.common.collect.Lists;
import com.google.gson.JsonIOException;
import com.planarshiftmc.plugin.planarmagic.PlanarConduit;

public class PhotonMappings {

	protected Map<Material, Integer> materialToXP;

	public PhotonMappings() {
		this.materialToXP = new HashMap<>();
		// look for yaml config file
		File configFile = new File(PlanarConduit.self().getDataFolder(), "photon_values.yml");
		FileConfiguration config = new YamlConfiguration();
		if (!configFile.exists()) {
			// Load default and save it out
			PlanarConduit.self().saveResource("photon_values.yml", false);
		}

		try {
			config.load(configFile);
		} catch (IOException | InvalidConfigurationException e) {
			PlanarConduit.self().getLogger().severe("Failed to load photon_values.yml! :: " + e.getMessage());
		}

		// Put Materials from config into map for (Material mat : Material.values())
		for (Material mat : Material.values()) {
			if (config.contains(mat.name())) {
				this.materialToXP.put(mat, config.getInt(mat.name()));
			}
		}

		// Calculate recipe values
		Collection<Recipe> recipes = Lists.newArrayList(Bukkit.recipeIterator());
		for (int i = 0; recipes.size() > 0 && i < 10; i++) {
			recipes = this.buildRecipeValues(recipes);
			PlanarConduit.self().getLogger().info(recipes.size() + " recipes still to be processed after pass " + i);
		}

		// Print any missed materials
		for (Material mat : Material.values()) {
			if (!this.materialToXP.containsKey(mat)) {
				PlanarConduit.self().getLogger().warning(mat.toString() + " could not be computed and is not set! Setting to 1.");
				this.materialToXP.put(mat, 1);
			}
		}
		// materialToXP.values().removeIf(entry -> !entry.equals(new Integer(1)));
		this.debugWriteoutPhotonMappings();
	}

	private void debugWriteoutPhotonMappings() {
		File debugFile = new File(PlanarConduit.self().getDataFolder(), "photon_debug.json");
		try (FileWriter writer = new FileWriter(debugFile)) {
			PlanarConduit.getGson().toJson(this.materialToXP, writer);
			writer.flush();
			writer.close();
		} catch (JsonIOException | IOException e) {
			PlanarConduit.self().getLogger().log(Level.SEVERE, "Debug file could not be written!", e);
		}
	}

	private Collection<Recipe> buildRecipeValues(Collection<Recipe> recipes) {
		// recipes
		for (Recipe r : recipes) {
			int val = 0;
			boolean missing = false;
			// loop through ingredients
			for (Entry<Material, Integer> ingredient : getIngredientCounts(r).entrySet()) {
				if (!this.materialToXP.containsKey(ingredient.getKey())) {
					missing = true;
				} else {
					val += this.materialToXP.get(ingredient.getKey()) * ingredient.getValue();
				}
			}
			if (!missing) {
				this.materialToXP.put(r.getResult().getType(), val);
			}
		}

		// remove items we now have the value for
		recipes.removeIf((Recipe r) -> {
			return this.materialToXP.containsKey(r.getResult().getType());
		});
		return recipes;
	}

	private Map<Material, Integer> getIngredientCounts(Recipe recipe) {
		Collection<ItemStack> stacks = new ArrayList<>();
		if (recipe instanceof FurnaceRecipe)
			stacks = Arrays.asList(((FurnaceRecipe) recipe).getInput());
		if (recipe instanceof ShapedRecipe)
			stacks = ((ShapedRecipe) recipe).getIngredientMap().values();
		if (recipe instanceof ShapelessRecipe)
			stacks = ((ShapelessRecipe) recipe).getIngredientList();

		Map<Material, Integer> ingredientCounts = new HashMap<>();
		for (ItemStack item : stacks) {
			if (item != null) {
				Material mat = item.getType();
				if (!ingredientCounts.containsKey(mat))
					ingredientCounts.put(mat, 1);
				else
					ingredientCounts.put(mat, ingredientCounts.get(mat) + 1);
			}
		}
		return ingredientCounts;
	}

	/**
	 * Retrieve the amount of photons for a material.
	 * 
	 * @param mat The material to look up.
	 * @return The amount of photons for the material.
	 */
	public int getPhotons(Material mat) {
		if (!this.materialToXP.containsKey(mat)) {
			throw new NoSuchElementException("Invalid Material :: " + mat.name());
		}
		return this.materialToXP.get(mat);
	}

	/**
	 * Retrieve the amount of photons for the item stack.
	 * 
	 * @param stack The ItemStack of an item.
	 * 
	 * @return The amount of photons created.
	 */
	public int getPhotons(ItemStack stack) {
		// TODO: Use meta data on object as an override.
		return this.getPhotons(stack.getType()) * stack.getAmount();
	}
}
