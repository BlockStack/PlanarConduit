package com.planarshiftmc.plugin.planarmagic;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.bukkit.entity.Player;

import com.planarshiftmc.plugin.planarmagic.companion.Companion;

/**
 * Occurs every tick to update companion location.
 * 
 * @author wispofates
 *
 */
public class CompanionMovementTimedTask implements Runnable {

	@Override
	public void run() {
		List<Player> goneOffline = new LinkedList<>();
		Iterator<Companion> it = PlanarConduit.self().getCompanions().getCompanions();
		while (it.hasNext()) {
			Companion companion = it.next();
			if (!companion.getPlayer().isOnline()) {
				goneOffline.add(companion.getPlayer());
			} else {
				companion.updateLocation();
			}
		}
		for (Player offline : goneOffline) {
			PlanarConduit.self().getCompanions().removeCompanion(offline.getUniqueId());
		}
	}
}
