package com.planarshiftmc.plugin.planarmagic.companion;

import java.util.Optional;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

import com.planarshiftmc.plugin.planarmagic.PlanarConduit;
import com.planarshiftmc.plugin.planarmagic.companion.Appearance.DIALOG;
import com.planarshiftmc.plugin.planarmagic.events.SpawnCompanionEvent;
import com.planarshiftmc.plugin.planarmagic.gson.GSONPostProcessable;

/**
 * 
 * @author wispoffates
 */
public class Companion implements GSONPostProcessable {
	
	protected UUID playerId;
	
	protected Optional<String> name = Optional.empty();
	protected boolean showName = true;
	protected boolean isVisible = true;
	protected String appearanceName;
	

	// transient (not saved in json and must be created each time)
	transient protected Player player;
	transient protected Location location;
	transient int enityId;
	transient protected ArmorStand armorCompanion;
	// transient protected ArmorStand armorName;
	
	/**
	 * GSON constructor
	 */
	protected Companion() {
		// GSON loads everything no need to do anything.
	}

	@Override
	public void gsonPostProcess() {
		this.player = Bukkit.getPlayer(this.playerId);
	}

	/**
	 * Constructor
	 * 
	 * @param player
	 *                   Player for the companion.
	 */
	public Companion(Player player) {
		this.player = player;
		this.playerId = player.getUniqueId();
		this.appearanceName = Appearances.defaultAppearanceName;
	}
	
	/**
	 * Spawn the entity into the world.
	 */
	public void spawn() {
		SpawnCompanionEvent event = new SpawnCompanionEvent(this, this.player);
		Bukkit.getServer().getPluginManager().callEvent(event);
		if (event.isCancelled()) {
			return;
		}
		if (!this.isVisible) {
			Bukkit.getPlayer(this.playerId).sendMessage("Companion is invisible.");
			return;
		}
		this.armorCompanion = this.player.getWorld().spawn(this.player.getLocation(), ArmorStand.class);
		this.armorCompanion.setMetadata("PlanarConduit", new FixedMetadataValue(PlanarConduit.self(), true));
		this.armorCompanion.addScoreboardTag("PlanarConduit");

		Appearance app = PlanarConduit.self().getAppearances().getAppearence(this.appearanceName);
		this.armorCompanion = app.setAppearance(this.armorCompanion);

		// this.armorCompanion.setCustomNameVisible(this.showName);
		this.armorCompanion.setCustomNameVisible(false);
		this.name.ifPresent((name) -> {
			this.armorCompanion.setCustomName(name);
		});
		this.enityId = this.armorCompanion.getEntityId();
		PlanarConduit.self().getCompanions().updateCompanion(this);

		// TODO: use name to check if we had to reset to default appearance and toss a
		// changeEvent.
	}

	/**
	 * Update the companions location and velocity.
	 */
	public void updateLocation() {
		Location newLoc = this.player.getLocation();
		newLoc.add(newLoc.getDirection().normalize().multiply(-1.2D)); // slight behind
		newLoc.add(0.0D, 1.0D, 0.0D); // slightly up
		newLoc.setYaw(this.player.getLocation().getYaw());
		newLoc.setPitch(this.player.getLocation().getPitch());
		// don't move the companion is close to the player (prevents jerking)
		if (this.armorCompanion.getLocation().distance(this.player.getLocation()) < 2) {
			return;
		}
		// teleport causes it to move should be less jarring then setting position.
		this.armorCompanion.teleport(newLoc);
		this.location = newLoc;
		// protocolSetBlock(this.location, Material.BLUE_GLAZED_TERRACOTTA);
	}

	/*
	 * private void protocolSetMetadata() { PacketContainer update = new
	 * PacketContainer(PacketType.Play.Server.ENTITY_METADATA);
	 * update.getIntegers().write(0, this.enityId);
	 * update.getWatchableCollectionModifier().write(0,
	 * WrappedDataWatcher.getEntityWatcher(player).getWatchableObjects()); try {
	 * PlanarConduit.self().getProtocol().sendServerPacket(player, update); } catch
	 * (InvocationTargetException e) {
	 * PlanarConduit.self().getLogger().severe("Failed to send block by protocol: "
	 * + e.getMessage()); } }
	 */

	/**
	 * Retrieve the current location of the companion/
	 * 
	 * @return The companion location.
	 */
	public Location getLocation() {
		return this.location;
	}

	/**
	 * Remove the companion.
	 */
	public void remove() {
		this.armorCompanion.remove();
	}

	/**
	 * @return Retrieve the companions owner.
	 */
	public Player getPlayer() {
		return this.player;
	}

	/**
	 * Passthrough message to retrieve dialog for the companions appearance.
	 * 
	 * @param dialog
	 *                   The dialog category.
	 * @return Optionally tehe dialog.
	 */
	public Optional<String> getDialog(DIALOG dialog) {
		Appearance appearance = PlanarConduit.self().getAppearances().getAppearence(this.appearanceName);
		return appearance.getDialog(dialog);
	}

	/**
	 * Retrieve base entity. Commonly used to check nearbyEntities.
	 * 
	 * @return The base entity.
	 */
	public Entity getEntity() {
		return this.armorCompanion;
	}
}
