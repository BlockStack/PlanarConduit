package com.planarshiftmc.plugin.planarmagic.companion;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import org.bukkit.Color;
import org.bukkit.Material;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.planarshiftmc.plugin.planarmagic.PlanarConduit;
import com.planarshiftmc.plugin.planarmagic.companion.Appearance.DIALOG;

/**
 * Handles loading and caching of the available appearances.
 * 
 * @author wispoffates
 */
public class Appearances {

	protected Map<String, Appearance> appearances;
	protected static String defaultAppearanceName;

	/**
	 * Constructor
	 */
	public Appearances() {
		this.appearances = new HashMap<>();
		this.loadAppearances();
		defaultAppearanceName = PlanarConduit.self().getConfig().getString("defaultAppearanceName");
	}

	/**
	 * Return the named appearnce or the default appearance if it is not available;
	 * 
	 * @param name
	 *                 The name of the appearance.
	 * @return The appearance or the default if not found.
	 */
	public Appearance getAppearence(String name) {
		Appearance app = this.appearances.get(name);
		if (app == null) {
			app = this.appearances.get(defaultAppearanceName);
			if (app == null) {
				PlanarConduit.self().getLogger().severe(MessageFormat
						.format("The default appeance $0 was not found in the appeance list!", defaultAppearanceName));
			}
		}
		return app;
	}

	/**
	 * Retrieve the name of the default appearance
	 * 
	 * @return The default appearance name.
	 */
	public String getDefaultAppearanceName() {
		return defaultAppearanceName;
	}

	/**
	 * Load the default spooky ghost.
	 */
	public void setDefaults() {
		Appearances.defaultAppearanceName = "Spooky Ghost";
		Appearance defaultApp = new Appearance(Appearances.defaultAppearanceName);
		AppearanceItem head = new AppearanceItem(Material.PLAYER_HEAD);
		head.url = "http://textures.minecraft.net/texture/92396b0ad352b2531139c9dc4cef9a32350c7cc14ed7bcfc5185ce15a7a7d5eb";
		defaultApp.helmet = head;
		AppearanceItem chest = new AppearanceItem(Material.LEATHER_CHESTPLATE);
		chest.setColor(Color.WHITE);
		defaultApp.setChestplate(chest);
		defaultApp.addDialog(DIALOG.IDLE, "Isn't there something you should be doing?");
		defaultApp.addDialog(DIALOG.INTERACT, "Need Something?");
		defaultApp.addDialog(DIALOG.INTERACT, "Whats up?");
		defaultApp.addDialog(DIALOG.INTERACT_OTHER, "That's racist.");
		this.appearances.put(defaultApp.name, defaultApp);
	}

	protected void loadAppearances() {
		File appearanceFolder = new File(PlanarConduit.self().getDataFolder(), "appearances");
		if (!appearanceFolder.exists()) {
			appearanceFolder.mkdirs();
		}
		File[] appFiles = appearanceFolder.listFiles();
		if (appFiles.length == 0) {
			// empty folder load and save the defaults
			this.setDefaults();
			this.saveAppearances();
			PlanarConduit.self().getLogger().info("Loaded default appearances.");
			return;
		}
		for (File appFile : appFiles) {
			try {
				Appearance app = PlanarConduit.getGson().fromJson(new FileReader(appFile), Appearance.class);
				this.appearances.put(app.getName(), app);
			} catch (JsonIOException | JsonSyntaxException e) {
				PlanarConduit.self().getLogger().log(Level.SEVERE,
						"Appearances file damaged backing up to appearances.corrupt and loading default appearances.",
						e);
				appFile.renameTo(new File(appFile, appFile.getName() + ".corrupt"));
			} catch (FileNotFoundException e) {
				// default all ready loaded
			}
		}
		PlanarConduit.self().getLogger().info("Loaded " + this.appearances.size() + " appearances.");
	}

	protected void saveAppearances() {
		File appearanceFolder = new File(PlanarConduit.self().getDataFolder(), "appearances");
		for (Appearance app : this.appearances.values()) {
			try (FileWriter writer = new FileWriter(new File(appearanceFolder, app.getName() + ".json"));) {
				PlanarConduit.getGson().toJson(app, writer);
				writer.flush();
				writer.close();
			} catch (JsonIOException | IOException e) {
				PlanarConduit.self().getLogger().log(Level.SEVERE,
						"Appearances file could not be written to " + app.toString() + "!", e);
			}
		}
	}

}
